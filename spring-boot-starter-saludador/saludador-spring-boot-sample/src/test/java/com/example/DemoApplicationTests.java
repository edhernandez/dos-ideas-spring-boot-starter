package com.example;

import com.dosideas.proyectoprueba.Saludador;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private Saludador saludador;
    
	@Test
	public void contextLoads() {
            System.out.println(saludador.saludar());
            Assert.assertEquals("Hola Eduardo", saludador.saludar());
	}

}
