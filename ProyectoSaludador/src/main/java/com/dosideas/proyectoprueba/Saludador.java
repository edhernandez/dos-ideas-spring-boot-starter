package com.dosideas.proyectoprueba;

public class Saludador {

    private String hola;
    private String nombre;

    public Saludador(String hola, String nombre) {
        this.hola = hola;
        this.nombre = nombre;
    }

    public String saludar() {
        return hola + " " + nombre;
    }

}
